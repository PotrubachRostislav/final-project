package com.epam;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

public class Application {

    public static void main(String[] args) throws Exception {
        FrontControllerServlet frontController = new FrontControllerServlet();

        ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
        context.addServlet(new ServletHolder(frontController), "/*");

        Server server = new Server(8080);
        server.setHandler(context);

        System.out.println("Change for gitHub");

        server.start();
        server.join();
    }
}
