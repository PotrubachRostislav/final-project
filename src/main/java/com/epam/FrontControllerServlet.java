package com.epam;


import com.epam.command.ICommand;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.epam.Constants.FRONT_CONTROLLER_SERVLET;

public class FrontControllerServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        requestProcess(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        requestProcess(req, resp);
    }

    private void requestProcess(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getRequestURI()
                .replaceAll(".*" + FRONT_CONTROLLER_SERVLET, "")
                .replaceAll("\\d+", "");
        ICommand command = Utils.commands.get(url);
        command.commandProcess(request, response);
    }


}
