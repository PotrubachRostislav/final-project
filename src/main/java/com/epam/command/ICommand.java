package com.epam.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ICommand {
    void commandProcess(HttpServletRequest request, HttpServletResponse response);
}
