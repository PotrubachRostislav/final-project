package com.epam;

import com.epam.command.ICommand;
import com.epam.command.Start;
import com.epam.command.impl.UsersCommand;

import java.util.HashMap;
import java.util.Map;

import static com.epam.Constants.*;

public class Utils {

    public static Map<String, ICommand> commands = new HashMap<>();

    static {
        commands.put(START_PROJECT, new Start());
        commands.put(USERS, new UsersCommand());

    }
}
