package com.epam.dbConnection;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class MySqlConnector {
//    private static final Logger LOGGER = LogManager.getLogger(MySQLConnectorManager.class);

    private static DataSource dataSource;

    static {
        PoolProperties p = new PoolProperties();
        p.setUrl("jdbc:mysql://localhost:3306/finalproject?serverTimezone=UTC");
//        p.setDriverClassName("com.mysql.cj.jdbc.Driver");
        p.setUsername("root");
        p.setPassword("0000");
        p.setTestWhileIdle(false);
        p.setTestOnBorrow(true);
        p.setValidationQuery("SELECT 1");
        p.setTestOnReturn(false);
        p.setValidationInterval(30000);
        p.setTimeBetweenEvictionRunsMillis(30000);
        p.setMaxActive(100);
        p.setInitialSize(10);
        p.setMaxWait(10000);
        p.setRemoveAbandonedTimeout(100);
        p.setMinEvictableIdleTimeMillis(30000);
        p.setMinIdle(10);
        p.setLogAbandoned(true);
        p.setRemoveAbandoned(true);
        p.setJdbcInterceptors(
                "org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer;"
                        + "org.apache.tomcat.jdbc.pool.interceptor.ResetAbandonedTimer");


        dataSource = new DataSource();
        dataSource.setPoolProperties(p);

    }

    /**
     * Gets connection java.sql.Connection from
     * connection pool using({@code DataSource} class)
     *
     * @return connection
     */
    public static synchronized Connection getConnection() {
        Connection connection = null;
        try {
            connection = dataSource.getConnection();

        } catch (SQLException e) {

//            LOGGER.error(e.getMessage());
        }
        return connection;
    }

    /**
     * Closes connection
     *
     * @param connection java.sql.Connection
     */
    public static void closeConnection(Connection connection) {
        try {
            connection.close();

        } catch (SQLException e) {

//            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Starts transaction by setting autocommit parameter to false
     *
     * @param connection java.sql.Connection
     */
    public static void startTransaction(Connection connection) {
        try {

            connection.setAutoCommit(false);

        } catch (SQLException e) {

//            LOGGER.error(e.getMessage());
        }
    }

    /**
     * Commits transaction
     *
     * @param connection java.sql.Connection
     */
    public static void commitTransaction(Connection connection) {

        try {
            connection.commit();
        } catch (SQLException e) {

//            LOGGER.error(e.getMessage());
            rollbackTransaction(connection);
        }
    }

    /**
     * Rollbacks transaction
     *
     * @param connection java.sql.Connection
     */
    private static void rollbackTransaction(Connection connection) {
        try {
            connection.rollback();

        } catch (SQLException e) {

//            LOGGER.error(e.getMessage());
        }
    }

}
