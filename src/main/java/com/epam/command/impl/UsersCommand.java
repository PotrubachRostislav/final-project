package com.epam.command.impl;

import com.epam.command.ICommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class UsersCommand implements ICommand {

    @Override
    public void commandProcess(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        try {
            response.getWriter().println("Users page");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
